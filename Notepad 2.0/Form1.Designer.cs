﻿namespace Notepad_2 . _0 {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System . ComponentModel . IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing ) {
			if ( disposing && ( components != null ) ) {
				components . Dispose ( );
			}
			base . Dispose ( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newCtrlNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newWindowCtrlShiftNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openCtrlOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveCtrlSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAsCtrlShiftSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.pageSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.printCtrlPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.undoCtrlZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.redoCtrlYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.cutCtrlXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.copyCtrlCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.pasteCtrlVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteDelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
			this.searchWithBingCtrlEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.findCtrlFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.findNextF3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.findPreviousShiftF3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.replaceCtrlHToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.goToCtrlGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
			this.selectAllCtrlAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.timeDateF5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.formatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.wordWrapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.fontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.zoomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.zoomInCtrlPlusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.zoomOutCtrlMinusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.restoreDefaultZoomCtrl0ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.statusBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.viewHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.sendFeedbackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.TxtBox = new System.Windows.Forms.TextBox();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.formatToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1354, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newCtrlNToolStripMenuItem,
            this.newWindowCtrlShiftNToolStripMenuItem,
            this.openCtrlOToolStripMenuItem,
            this.saveCtrlSToolStripMenuItem,
            this.saveAsCtrlShiftSToolStripMenuItem,
            this.toolStripSeparator2,
            this.pageSetupToolStripMenuItem,
            this.printCtrlPToolStripMenuItem,
            this.toolStripSeparator3,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// newCtrlNToolStripMenuItem
			// 
			this.newCtrlNToolStripMenuItem.Name = "newCtrlNToolStripMenuItem";
			this.newCtrlNToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
			this.newCtrlNToolStripMenuItem.Text = "New [Ctrl+N]";
			this.newCtrlNToolStripMenuItem.Click += new System.EventHandler(this.NewClicked);
			// 
			// newWindowCtrlShiftNToolStripMenuItem
			// 
			this.newWindowCtrlShiftNToolStripMenuItem.Name = "newWindowCtrlShiftNToolStripMenuItem";
			this.newWindowCtrlShiftNToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
			this.newWindowCtrlShiftNToolStripMenuItem.Text = "New Window [Ctrl+Shift+N]";
			// 
			// openCtrlOToolStripMenuItem
			// 
			this.openCtrlOToolStripMenuItem.Name = "openCtrlOToolStripMenuItem";
			this.openCtrlOToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
			this.openCtrlOToolStripMenuItem.Text = "Open... [Ctrl+O]";
			this.openCtrlOToolStripMenuItem.Click += new System.EventHandler(this.OpenClicked);
			// 
			// saveCtrlSToolStripMenuItem
			// 
			this.saveCtrlSToolStripMenuItem.Name = "saveCtrlSToolStripMenuItem";
			this.saveCtrlSToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
			this.saveCtrlSToolStripMenuItem.Text = "Save [Ctrl+S]";
			this.saveCtrlSToolStripMenuItem.Click += new System.EventHandler(this.SaveClicked);
			// 
			// saveAsCtrlShiftSToolStripMenuItem
			// 
			this.saveAsCtrlShiftSToolStripMenuItem.Name = "saveAsCtrlShiftSToolStripMenuItem";
			this.saveAsCtrlShiftSToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
			this.saveAsCtrlShiftSToolStripMenuItem.Text = "Save As... [Ctrl+Shift+S]";
			this.saveAsCtrlShiftSToolStripMenuItem.Click += new System.EventHandler(this.SaveAsClicked);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(221, 6);
			// 
			// pageSetupToolStripMenuItem
			// 
			this.pageSetupToolStripMenuItem.Name = "pageSetupToolStripMenuItem";
			this.pageSetupToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
			this.pageSetupToolStripMenuItem.Text = "Page Setup...";
			// 
			// printCtrlPToolStripMenuItem
			// 
			this.printCtrlPToolStripMenuItem.Name = "printCtrlPToolStripMenuItem";
			this.printCtrlPToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
			this.printCtrlPToolStripMenuItem.Text = "Print... [Ctrl+P]";
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(221, 6);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
			this.exitToolStripMenuItem.Text = "Exit";
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoCtrlZToolStripMenuItem,
            this.redoCtrlYToolStripMenuItem,
            this.toolStripSeparator4,
            this.cutCtrlXToolStripMenuItem,
            this.copyCtrlCToolStripMenuItem,
            this.pasteCtrlVToolStripMenuItem,
            this.deleteDelToolStripMenuItem,
            this.toolStripSeparator5,
            this.searchWithBingCtrlEToolStripMenuItem,
            this.findCtrlFToolStripMenuItem,
            this.findNextF3ToolStripMenuItem,
            this.findPreviousShiftF3ToolStripMenuItem,
            this.replaceCtrlHToolStripMenuItem,
            this.goToCtrlGToolStripMenuItem,
            this.toolStripSeparator6,
            this.selectAllCtrlAToolStripMenuItem,
            this.timeDateF5ToolStripMenuItem});
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
			this.editToolStripMenuItem.Text = "Edit";
			// 
			// undoCtrlZToolStripMenuItem
			// 
			this.undoCtrlZToolStripMenuItem.Name = "undoCtrlZToolStripMenuItem";
			this.undoCtrlZToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
			this.undoCtrlZToolStripMenuItem.Text = "Undo [Ctrl+Z]";
			this.undoCtrlZToolStripMenuItem.Click += new System.EventHandler(this.UndoClicked);
			// 
			// redoCtrlYToolStripMenuItem
			// 
			this.redoCtrlYToolStripMenuItem.Name = "redoCtrlYToolStripMenuItem";
			this.redoCtrlYToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
			this.redoCtrlYToolStripMenuItem.Text = "Redo [Ctrl+Y]";
			this.redoCtrlYToolStripMenuItem.Click += new System.EventHandler(this.RedoClicked);
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(212, 6);
			// 
			// cutCtrlXToolStripMenuItem
			// 
			this.cutCtrlXToolStripMenuItem.Name = "cutCtrlXToolStripMenuItem";
			this.cutCtrlXToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
			this.cutCtrlXToolStripMenuItem.Text = "Cut [Ctrl+X]";
			// 
			// copyCtrlCToolStripMenuItem
			// 
			this.copyCtrlCToolStripMenuItem.Name = "copyCtrlCToolStripMenuItem";
			this.copyCtrlCToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
			this.copyCtrlCToolStripMenuItem.Text = "Copy [Ctrl+C]";
			// 
			// pasteCtrlVToolStripMenuItem
			// 
			this.pasteCtrlVToolStripMenuItem.Name = "pasteCtrlVToolStripMenuItem";
			this.pasteCtrlVToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
			this.pasteCtrlVToolStripMenuItem.Text = "Paste [Ctrl+V]";
			// 
			// deleteDelToolStripMenuItem
			// 
			this.deleteDelToolStripMenuItem.Name = "deleteDelToolStripMenuItem";
			this.deleteDelToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
			this.deleteDelToolStripMenuItem.Text = "Delete [Del]";
			// 
			// toolStripSeparator5
			// 
			this.toolStripSeparator5.Name = "toolStripSeparator5";
			this.toolStripSeparator5.Size = new System.Drawing.Size(212, 6);
			// 
			// searchWithBingCtrlEToolStripMenuItem
			// 
			this.searchWithBingCtrlEToolStripMenuItem.Name = "searchWithBingCtrlEToolStripMenuItem";
			this.searchWithBingCtrlEToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
			this.searchWithBingCtrlEToolStripMenuItem.Text = "Search with Bing... [Ctrl+E]";
			// 
			// findCtrlFToolStripMenuItem
			// 
			this.findCtrlFToolStripMenuItem.Name = "findCtrlFToolStripMenuItem";
			this.findCtrlFToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
			this.findCtrlFToolStripMenuItem.Text = "Find... [Ctrl+F]";
			// 
			// findNextF3ToolStripMenuItem
			// 
			this.findNextF3ToolStripMenuItem.Name = "findNextF3ToolStripMenuItem";
			this.findNextF3ToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
			this.findNextF3ToolStripMenuItem.Text = "Find Next [F3]";
			// 
			// findPreviousShiftF3ToolStripMenuItem
			// 
			this.findPreviousShiftF3ToolStripMenuItem.Name = "findPreviousShiftF3ToolStripMenuItem";
			this.findPreviousShiftF3ToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
			this.findPreviousShiftF3ToolStripMenuItem.Text = "Find Previous [Shift+F3]";
			// 
			// replaceCtrlHToolStripMenuItem
			// 
			this.replaceCtrlHToolStripMenuItem.Name = "replaceCtrlHToolStripMenuItem";
			this.replaceCtrlHToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
			this.replaceCtrlHToolStripMenuItem.Text = "Replace... [Ctrl+H]";
			// 
			// goToCtrlGToolStripMenuItem
			// 
			this.goToCtrlGToolStripMenuItem.Name = "goToCtrlGToolStripMenuItem";
			this.goToCtrlGToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
			this.goToCtrlGToolStripMenuItem.Text = "Go To... [Ctrl+G]";
			// 
			// toolStripSeparator6
			// 
			this.toolStripSeparator6.Name = "toolStripSeparator6";
			this.toolStripSeparator6.Size = new System.Drawing.Size(212, 6);
			// 
			// selectAllCtrlAToolStripMenuItem
			// 
			this.selectAllCtrlAToolStripMenuItem.Name = "selectAllCtrlAToolStripMenuItem";
			this.selectAllCtrlAToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
			this.selectAllCtrlAToolStripMenuItem.Text = "Select All [Ctrl+A]";
			// 
			// timeDateF5ToolStripMenuItem
			// 
			this.timeDateF5ToolStripMenuItem.Name = "timeDateF5ToolStripMenuItem";
			this.timeDateF5ToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
			this.timeDateF5ToolStripMenuItem.Text = "Time/Date [F5]";
			// 
			// formatToolStripMenuItem
			// 
			this.formatToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wordWrapToolStripMenuItem,
            this.fontToolStripMenuItem});
			this.formatToolStripMenuItem.Name = "formatToolStripMenuItem";
			this.formatToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
			this.formatToolStripMenuItem.Text = "Format";
			// 
			// wordWrapToolStripMenuItem
			// 
			this.wordWrapToolStripMenuItem.Name = "wordWrapToolStripMenuItem";
			this.wordWrapToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.wordWrapToolStripMenuItem.Text = "Word Wrap";
			// 
			// fontToolStripMenuItem
			// 
			this.fontToolStripMenuItem.Name = "fontToolStripMenuItem";
			this.fontToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.fontToolStripMenuItem.Text = "Font";
			this.fontToolStripMenuItem.Click += new System.EventHandler(this.FontClicked);
			// 
			// viewToolStripMenuItem
			// 
			this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zoomToolStripMenuItem,
            this.statusBarToolStripMenuItem});
			this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
			this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.viewToolStripMenuItem.Text = "View";
			// 
			// zoomToolStripMenuItem
			// 
			this.zoomToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zoomInCtrlPlusToolStripMenuItem,
            this.zoomOutCtrlMinusToolStripMenuItem,
            this.restoreDefaultZoomCtrl0ToolStripMenuItem});
			this.zoomToolStripMenuItem.Name = "zoomToolStripMenuItem";
			this.zoomToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
			this.zoomToolStripMenuItem.Text = "Zoom";
			// 
			// zoomInCtrlPlusToolStripMenuItem
			// 
			this.zoomInCtrlPlusToolStripMenuItem.Name = "zoomInCtrlPlusToolStripMenuItem";
			this.zoomInCtrlPlusToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
			this.zoomInCtrlPlusToolStripMenuItem.Text = "Zoom In [Ctrl+Plus]";
			// 
			// zoomOutCtrlMinusToolStripMenuItem
			// 
			this.zoomOutCtrlMinusToolStripMenuItem.Name = "zoomOutCtrlMinusToolStripMenuItem";
			this.zoomOutCtrlMinusToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
			this.zoomOutCtrlMinusToolStripMenuItem.Text = "Zoom Out [Ctrl+Minus]";
			// 
			// restoreDefaultZoomCtrl0ToolStripMenuItem
			// 
			this.restoreDefaultZoomCtrl0ToolStripMenuItem.Name = "restoreDefaultZoomCtrl0ToolStripMenuItem";
			this.restoreDefaultZoomCtrl0ToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
			this.restoreDefaultZoomCtrl0ToolStripMenuItem.Text = "Restore Default Zoom [Ctrl+0]";
			// 
			// statusBarToolStripMenuItem
			// 
			this.statusBarToolStripMenuItem.Name = "statusBarToolStripMenuItem";
			this.statusBarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
			this.statusBarToolStripMenuItem.Text = "Status Bar";
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewHelpToolStripMenuItem,
            this.sendFeedbackToolStripMenuItem,
            this.toolStripSeparator1,
            this.aboutToolStripMenuItem});
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.helpToolStripMenuItem.Text = "Help";
			// 
			// viewHelpToolStripMenuItem
			// 
			this.viewHelpToolStripMenuItem.Name = "viewHelpToolStripMenuItem";
			this.viewHelpToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
			this.viewHelpToolStripMenuItem.Text = "View Help";
			// 
			// sendFeedbackToolStripMenuItem
			// 
			this.sendFeedbackToolStripMenuItem.Name = "sendFeedbackToolStripMenuItem";
			this.sendFeedbackToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
			this.sendFeedbackToolStripMenuItem.Text = "Send Feedback";
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(171, 6);
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
			this.aboutToolStripMenuItem.Text = "About Notepad 2.0";
			// 
			// TxtBox
			// 
			this.TxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TxtBox.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.TxtBox.Location = new System.Drawing.Point(0, 23);
			this.TxtBox.Multiline = true;
			this.TxtBox.Name = "TxtBox";
			this.TxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.TxtBox.Size = new System.Drawing.Size(1354, 596);
			this.TxtBox.TabIndex = 1;
			this.TxtBox.TextChanged += new System.EventHandler(this.TxtChanged);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1354, 619);
			this.Controls.Add(this.TxtBox);
			this.Controls.Add(this.menuStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "Form1";
			this.Text = "Untitled - Notepad 2.0";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormClosingEvent);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormKeyDown);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormKeyUp);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System . Windows . Forms . MenuStrip menuStrip1;
		private System . Windows . Forms . ToolStripMenuItem fileToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem editToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem formatToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem viewToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem helpToolStripMenuItem;
		private System . Windows . Forms . TextBox TxtBox;
		private System . Windows . Forms . ToolStripMenuItem newCtrlNToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem newWindowCtrlShiftNToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem openCtrlOToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem saveCtrlSToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem saveAsCtrlShiftSToolStripMenuItem;
		private System . Windows . Forms . ToolStripSeparator toolStripSeparator2;
		private System . Windows . Forms . ToolStripMenuItem pageSetupToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem printCtrlPToolStripMenuItem;
		private System . Windows . Forms . ToolStripSeparator toolStripSeparator3;
		private System . Windows . Forms . ToolStripMenuItem exitToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem viewHelpToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem sendFeedbackToolStripMenuItem;
		private System . Windows . Forms . ToolStripSeparator toolStripSeparator1;
		private System . Windows . Forms . ToolStripMenuItem aboutToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem undoCtrlZToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem redoCtrlYToolStripMenuItem;
		private System . Windows . Forms . ToolStripSeparator toolStripSeparator4;
		private System . Windows . Forms . ToolStripMenuItem cutCtrlXToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem copyCtrlCToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem pasteCtrlVToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem deleteDelToolStripMenuItem;
		private System . Windows . Forms . ToolStripSeparator toolStripSeparator5;
		private System . Windows . Forms . ToolStripMenuItem searchWithBingCtrlEToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem findCtrlFToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem findNextF3ToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem findPreviousShiftF3ToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem replaceCtrlHToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem goToCtrlGToolStripMenuItem;
		private System . Windows . Forms . ToolStripSeparator toolStripSeparator6;
		private System . Windows . Forms . ToolStripMenuItem selectAllCtrlAToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem timeDateF5ToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem wordWrapToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem fontToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem zoomToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem zoomInCtrlPlusToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem zoomOutCtrlMinusToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem restoreDefaultZoomCtrl0ToolStripMenuItem;
		private System . Windows . Forms . ToolStripMenuItem statusBarToolStripMenuItem;
	}
}

