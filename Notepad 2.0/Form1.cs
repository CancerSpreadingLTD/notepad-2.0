﻿#define SINGLEFILE
using System;
using System . Collections . Generic;
using System . ComponentModel;
using System . Data;
using System . Drawing;
using System . Linq;
using System . Text;
using System . Threading . Tasks;
using System . Windows . Forms;

namespace Notepad_2 . _0 {
	public partial class Form1 : Form {
		public Form1() {
			InitializeComponent ( );
			InitConfig ( );
			this . KeyPreview = true;
			TxtBox . KeyDown += ( object sender , KeyEventArgs e ) => {
				if ( e . Modifiers == Keys . Control ) {
					switch ( e . KeyCode ) {
						case Keys . Z:
							UndoTXT ( );
							break;
						case Keys . Y:
							RedoTXT ( );
							break;
						case Keys . I:
						case Keys . H:
							e . Handled = true;
							e . SuppressKeyPress = true;
							break;
					}
				}
			};


			Shortcuts . Add ( new Keys[] { Keys . ControlKey , Keys . O } , () => { Open ( ); } );
			Shortcuts . Add ( new Keys[] { Keys . ControlKey , Keys . S } , () => { Save ( ); } );
			//Shortcuts . Add ( new Keys[] { Keys . ControlKey , Keys . Z } , () => { UndoTXT ( ); } );
			Shortcuts . Add ( new Keys[] { Keys . ControlKey , Keys . D4 } , () => { string DBG = "";
				foreach ( var X in Undo ) {
					DBG += X + "\r\n";

				}
				DBG += "\r\nREDO:\r\n";
				foreach ( var X in Redo ) {
					DBG += X + "\r\n";

				}
				MessageBox . Show ( DBG );
			} );
			Shortcuts . Add ( new Keys[] { Keys . ControlKey , Keys . E } , () => { SearchWithBing ( ); } );
			Shortcuts . Add ( new Keys[] { Keys . ControlKey , Keys . ShiftKey , Keys . S } , () => { SaveAs ( ); } );
		}

		private Stack<string> Undo = new Stack<string> ( );
		private Stack<string> Redo = new Stack<string> ( );

		private List<Keys> KeysDown = new List<Keys> ( );

		private Dictionary<Keys[] , Action> Shortcuts = new Dictionary<Keys[] , Action> ( );

		private bool DisableUndo = false;

		private bool DisableRedoClear = false;

		#region EVENT HANDLERS

		private void UndoClicked( Object sender , EventArgs e ) {
			UndoTXT ( );
		}

		private void RedoClicked( Object sender , EventArgs e ) {
			RedoTXT ( );
		}

		private void FontClicked( Object sender , EventArgs e ) {
			SelectTxtFont ( );
		}

		/// <summary>
		/// Form is currently closing.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FormClosingEvent( Object sender , FormClosingEventArgs e ) {
			SaveWindowCFG ( );
		}

		/// <summary>
		/// Previewed key down.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FormKeyDown( Object sender , KeyEventArgs e ) {
			switch ( KeysDown . Contains ( e . KeyCode ) ) {
				case false:
					KeysDown . Add ( e . KeyCode );
					CheckKbdShortcuts ( );
					break;
				default:
					break;
			}
		}

		/// <summary>
		/// Previewed key up.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FormKeyUp( Object sender , KeyEventArgs e ) {
			switch ( KeysDown . Contains ( e . KeyCode ) ) {
				case true:
					KeysDown . Remove ( e . KeyCode );
					break;
				default:
					break;
			}
		}

		/// <summary>
		/// Open clicked.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OpenClicked( Object sender , EventArgs e ) {
			Open ( );
		}

		/// <summary>
		/// Save as clicked.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void SaveAsClicked( Object sender , EventArgs e ) {
			SaveAs ( );
		}

		/// <summary>
		/// TxtBox Text was changed.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void TxtChanged( Object sender , EventArgs e ) {
			Changes = true;
			UpdateTitleBar ( );
			TxtBox . ClearUndo ( );
			switch ( DisableUndo ) {
				case false:
					Undo . Push ( TxtBox . Text );
					switch ( DisableRedoClear ) {
						case false:
							Redo . Clear ( );
							break;
					}
					
					break;
				default:
					break;
			}
			
		}

		/// <summary>
		/// Save was clicked.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void SaveClicked( Object sender , EventArgs e ) {
			Save ( );
		}

		/// <summary>
		/// New document.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void NewClicked( Object sender , EventArgs e ) {
			TxtBox . Text = "";
			Changes = false;
			OpenedFile = "Untitled";
			UpdateTitleBar ( );

		}

		#endregion

		private void RedoTXT() {
			switch ( Redo.Count != 0 ) {
				case true:
					DisableRedoClear = true;
					Redo . Pop ( );
					switch ( Redo . Count != 0 ) {
						case true:
							TxtBox . Text = Redo . Pop ( );
							break;
					}
					DisableRedoClear = false;
					TxtBox . SelectionStart = TxtBox . Text . Length;
					TxtBox . SelectionLength = 0;
					TxtBox . Focus ( );
					break;
			}

		}

		private void UndoTXT() {
			switch ( Undo . Count != 0 ) {
				case true:
					DisableUndo = true;
					var U = Undo . Pop ( );
					Redo . Push ( U );
					TxtBox . Text = U;
					TxtBox . SelectionStart = TxtBox . Text . Length;
					TxtBox . SelectionLength = 0;
					TxtBox . Focus ( );
					DisableUndo = false;
					break;
				case false:
					DisableRedoClear = true;
					TxtBox . Text = "";
					DisableRedoClear = false;
					break;
			}
		}

		private void SelectTxtFont() {
			FontDialog FNT = new FontDialog ( );
			FNT . Font = TxtBox . Font; 
			DialogResult DR = FNT . ShowDialog ( );
			switch ( DR ) {
				case DialogResult . OK:
					SetFont(FNT . Font);
					break;
				case DialogResult . Cancel:
					break;
				default:
					break;
			}
		}

		private void SetFont(Font F) {
			TxtBox . Font = F;
		}

		private void InitConfig(){
#if SINGLEFILE
			switch ( System.IO.File.Exists("CONFIG.CFG") ) {
				case false:
					System . IO . File . WriteAllText ( "CONFIG.CFG","" );
				break;
			}
#endif
			ConfigReader CR = new ConfigReader ( "CONFIG.CFG" );
			int WHeight = (int)CR . Get ( "WND_H" );
			int WWidth = (int)CR . Get ( "WND_W" );
			switch ( WHeight!=0 ) {
				case true:
					this . Height = WHeight;
					break;
			}
			switch ( WWidth != 0 ) {
				case true:
					this . Width = WWidth;
					break;
			}
		}

		private void SaveWindowCFG() {
			ConfigWriter CW = new ConfigWriter ( "CONFIG.CFG" );
			CW . ChangeValue ( "WND_H" , this . Height + "" );
			CW . ChangeValue ( "WND_W" , this . Width + "" );
			CW . Write ( "CONFIG.CFG" );
		}

		/// <summary>
		/// Search selected text with Bing (TM)
		/// </summary>
		private void SearchWithBing() {
			System . Diagnostics . Process . Start ( "https://www.bing.com/search?q=" + TxtBox . SelectedText );
		}

		/// <summary>
		/// Check if any of KBD Shortcuts should be triggered.
		/// </summary>
		private void CheckKbdShortcuts() {
			foreach ( var X in Shortcuts ) {
				foreach (var Y in X . Key ) {
					switch ( KeysDown . Contains ( Y ) ) {
						case false:
							goto ShortcutFailed;
					}
				} // ENDOF: FE var Y in X.Key (Shortcuts)
				foreach ( var Z in X . Key ) {
					KeysDown . Remove ( Z );
				}
				X . Value . Invoke ( );
			ShortcutFailed:;
			} // ENDOF: FE var X in Shortcuts
		
		}

		/// <summary>
		/// Save current file.
		/// </summary>
		private void Save() {
			switch ( OpenedFile ) {
				case "Untitled":
					SaveAs ( );
					break;
				default:
					System . IO . File . WriteAllText ( OpenedFile , TxtBox . Text );
					Changes = false;
					UpdateTitleBar ( );
					break;
			}
		}

		/// <summary>
		/// Save file as.
		/// </summary>
		private void SaveAs() {
			string F = SaveFile ( "Text Documents (*.txt)|*.TXT|Al Files|*.*" );
			if ( F != null ) { 
					System . IO . File . WriteAllText ( F, TxtBox . Text );
					Changes = false;
					OpenedFile = F;
					UpdateTitleBar ( );
			}
		}

		/// <summary>
		/// Open a file.
		/// </summary>
		private void Open() {
			string F = OpenFile ( "Text Documents (*.txt)|*.TXT|Al Files|*.*" );
			switch ( F ) {
				case null:
					break;
				default:
					OpenedFile = F;
					TxtBox . Text = System . IO . File . ReadAllText ( F );
					Changes = false;
					Undo . Clear ( );
					Undo . Push ( "" );
					Undo . Push ( TxtBox.Text );
					Redo . Clear ( );
					UpdateTitleBar ( );
					break;
			}
		}

		private string OpenedFile = "Untitled";

		private bool Changes = false;

		/// <summary>
		/// Update Form Text including * if any changes were made to the TxtBox
		/// </summary>
		private void UpdateTitleBar() {
			this . Text = ( Changes ? " *" : "" ) + System . IO . Path . GetFileName ( OpenedFile ) + " - Notepad 2.0";
			
		}

		/// <summary>
		/// Show SaveFileDialog and return full filename if Save was clicked.
		/// </summary>
		/// <param name="Filter"></param>
		/// <returns></returns>
		private string SaveFile( string Filter ) {
			string Result = null;
			SaveFileDialog SF = new SaveFileDialog ( );
			SF . Filter = Filter;
			DialogResult R = SF . ShowDialog ( );
			switch ( R ) {
				case DialogResult . OK:
					Result = SF . FileName;
					break;
				default:
					break;
			}
			return Result;
		}

		/// <summary>
		/// Show OpenFileDialog and return a full filename if Open was clicked.
		/// </summary>
		/// <param name="Filter"></param>
		/// <returns></returns>
		private string OpenFile( string Filter){
			string Result = null;
			OpenFileDialog OF = new OpenFileDialog ( );
			OF . Filter = Filter;
			OF . Multiselect = false;
			DialogResult R = OF . ShowDialog ( );
			switch ( R ) {
				case DialogResult . OK:
					Result = OF . FileName;
					break;
				default:
					break;
			}
			return Result;
		}


	}
}
