﻿using System;
using System . Collections . Generic;
using System . IO;
using System . Linq;
using System . Text;
using System . Threading . Tasks;

namespace Notepad_2._0 {
	public class ConfigReader {
		/// <summary>
		/// Initializes a new instance of ConfigReader class.
		/// </summary>
		/// <param name="FileName">.CFG file path.</param>
		public ConfigReader( string FileName ) {
			string[] TheFile = File . ReadAllLines ( FileName );

			foreach ( var input in TheFile ) {
				var x = input . Trim ( );
				if ( !x . StartsWith ( ";" ) & !( x == "" ) ) {

					var Line = x . Split ( ';' )[0];

					var Lix = Line . Split ( '=' );

					var Key = Lix[0] . Trim ( );

					var Value = Lix[1] . Trim ( );
					//Console.WriteLine("DFS");
					if ( !LoadedValues . ContainsKey ( Key ) )
						try {
							// Console.WriteLine(Key + ", " + Value);
							LoadedValues . Add ( Key , Value );
						} catch { } else
						try {
							// Console.WriteLine(Key + ", " + Value);
							LoadedValues[Key] = Value;
						} catch { }
				}
			}
		}

		/// <summary>
		/// Get value from file. Explicitly convertible to int, bool, string and byte.
		/// (Key = Value ; Comment)
		/// <para>Usage: Variable = (type)CfgLoader.Get("Key");</para>
		/// <para>Example: <para>
		/// ConfigLoader Loader = new ConfigLoader("Scripts\\Config.CFG");
		/// </para>Int32 MaxCakesSpawnable = (int)Loader.Get("MaxCakes");</para>
		/// </summary>
		/// <param name="Key">Item key.</param>
		/// <returns>Value</returns>
		public CfgItem Get( string Key ) {
			return LoadedValues . ContainsKey ( Key ) ? new CfgItem ( LoadedValues[Key] ) : null;
		}

		private Dictionary<string , string> LoadedValues = new Dictionary<string , string> ( );

	}
	
	public class ConfigWriter {
		public ConfigWriter( string FileName ) {
			if ( System . IO . File . Exists ( FileName ) )
				File = System . IO . File . ReadAllLines ( FileName );
			else {
				File = new string[1] { "" };
				System . IO . File . WriteAllText ( FileName , "" );
			}
		}

		public void ChangeValue( string Key , string Value ) {
			var Done = false;
			for ( Int32 I = 0 ; I < File . Length ; ++I ) {
				var x = File[I];
				//System . Windows . Forms . MessageBox . Show ( "FOund" );
				if ( x . Trim ( ) . ToUpper ( ) . StartsWith ( Key ) ) {
					var e = x . Split ( '=' );
					var f = e[1] . Split ( ';' );
					f[0] = Value;
					string Final = "";
					Final += e[0] . Trim ( ) + " = " + f[0];
					if ( f . Length > 1 ) {
						Final += " ; " + f[1] . Trim ( );
					}
					File[I] = Final;
					Done = true;
				}
			}

			if ( Done )
				return;

			if ( Append . ContainsKey ( Key . ToUpper ( ) ) ) {
				Append[Key . ToUpper ( )] = Value;
			} else {
				Append . Add ( Key , Value );
			}

		}


		public void DeleteValue( string Key ) {
			for ( Int32 I = 0 ; I < File . Length ; ++I ) {
				var x = File[I];
				if ( x . Trim ( ) . ToUpper ( ) . StartsWith ( Key ) ) {
					File[I] = "";
				}
			}

		}

		internal Dictionary<string , string> Append = new Dictionary<string , string> ( );

		public void Write( string FileName ) {
			List<string> _List = new List<string> ( );

			foreach ( var x in File ) {
				if ( x != "" )
					_List . Add ( x );
			}

			foreach ( var x in Append ) {
				_List . Add ( x . Key + " = " + x . Value );
			}

			System . IO . File . WriteAllLines ( FileName , _List );

		}



		string[] File;


	}
	
	/// <summary>
	/// Used to simplify the conversions.
	/// </summary>
	public class CfgItem {
		internal CfgItem( string _Value ) {
			Value = _Value;
		}
		internal string Value { get; set; }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="Item"></param>
		public static explicit operator string( CfgItem Item ) {
			return Item != null ? Item . Value : string . Empty;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="Item"></param>
		public static explicit operator bool( CfgItem Item ) {
			return Item != null ? Convert . ToBoolean ( Item . Value . Trim ( ) ) : false;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="Item"></param>
		public static explicit operator int( CfgItem Item ) {
			return Item != null ? Convert . ToInt32 ( Item . Value ) : 0;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="Item"></param>
		public static explicit operator byte( CfgItem Item ) {
			return Item != null ? Convert . ToByte ( Item . Value ) : (byte)0;
		}


	}

}
